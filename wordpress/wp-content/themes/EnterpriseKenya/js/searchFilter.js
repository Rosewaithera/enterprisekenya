document.addEventListener('DOMContentLoaded', searchEK);
var category_id,
    date_from,
    date_to,
    keyword;
function searchEK(){
// <<<<<<< HEAD

	(function($){

		//wp-pagenavi add blue /green classes
		$('.pagination-blue').each(function() {
		   $(this).find('.wp-pagenavi').addClass('pagenavi-blue');
		 });
		$('.pagination-green').each(function() {
		   $(this).find('.wp-pagenavi').addClass('pagenavi-green');
		 });

		var admin_ajax = home_url.url + "/wp-admin/admin-ajax.php";

		//load news
		$.post( admin_ajax, { action : 'ekFilterNews', query_vars: admin_ajax.query_vars }, function(html){
		$('#ek-news-posts').html(html);	
		});	

		//load events
		$.post( admin_ajax, { action : 'ekFilterEvents', query_vars: admin_ajax.query_vars }, function(html){
		$('#ek-events-posts').html(html);	
		});

		//load innovation
		$.post( admin_ajax, { action : 'ekFilterInnovation', query_vars: admin_ajax.query_vars }, function(html){
		$('#ek-innovation-posts').html(html);	
		});

		$(document).on('click','body', function(event){

			$(".ek-suggest-dropdown").empty();
			$('.search-ek').val('');

		})
		$(document).on('input','#search_ek', function(event){
			event.preventDefault();	

			var search_value = $(this).val();
					
		 	$.ajax({
		 		url: admin_ajax,
		 		type: 'post',
		 		data: {
		 			action: 'ekSearchAllSite',
		 			searchQuery: search_value
		 		},
		 		dataType:'html',

		 		success: function(html) {

		 		$("#ek-search-results").html(html);
					
		 		}
		 	})

		 })

		//get categories 
		$(document).on('change', '#news-category-list, #events-category-list', function(){

			window.category_id = $(this).val();						
		})


		//get page number
		function page_number(element){

			element.find('page smaller').remove();
			return parseInt( element.html() );

		}

		$(document).on('click','#filter-news-button, .pagination a', function(event){
			event.preventDefault();	

			var page  = page_number($(this).clone());
			
			if($('#news-category-list :selected').val() == ''){
				window.category_id = '';
			}
			
			var keyword = $('#news-keyword').val();
			var date_from = $('#news-date-from').val();
			var date_to = $('#news-date-to').val();

			$.ajax({
		 		url: admin_ajax,
		 		type: 'post',
		 		data: {
		 			action: 'ekFilterNews',
		 			query_vars: admin_ajax.query_vars,
		 			keyword : keyword,
		 			date_from : date_from,
		 			date_to : date_to,
		 			category_id : window.category_id,
		 			page : page
		 		},
		 		dataType:'html',

		 		success: function(html) {
		 			 // $('input').val('');
		 			 // $('option').attr('selected', false);
		 			
		 		$('#ek-news-posts').html(html);				
		 		}
		 	})

		 })

		$(document).on('click','#filter-events-button, .pagination a', function(event){
			event.preventDefault();	
			var page  = page_number($(this).clone());
			
			if($('#events-category-list :selected').val() == ''){
				window.category_id = '';
			}
			
			var events_keyword = $('#events-keyword').val();
			var events_date_from = $('#events-date-from').val();
			var events_date_to = $('#events-date-to').val();

			$.ajax({
		 		url: admin_ajax,
		 		type: 'post',
		 		data: {
		 			action: 'ekFilterEvents',
		 			query_vars: admin_ajax.query_vars,
		 			keyword : events_keyword,
		 			date_from : events_date_from,
		 			date_to : events_date_to,
		 			category_id : window.category_id,
		 			page : page
		 		},
		 		dataType:'html',

		 		success: function(html) {
		 			 // $('input').val('');
		 			 // $('option').attr('selected', false);
		 			
		 		$('#ek-events-posts').html(html);			
		 		}
		 	})

		 })
		

				
	
	
	 $(document).on('click','#filter-faq-button, .pagination a', function(event){
            event.preventDefault(); 
            var page  = page_number($('.pagination a')) || 1;            
            var faq_keyword = $('#faq-keyword').val();
           
            $.ajax({
                url: admin_ajax,
                type: 'post',
                data: {
                    action: 'ekFilterFaq',       
                    keyword :faq_keyword,                
                    page : page
                },
                dataType:'html',
                success: function(html) {
                
 
                $('.accordion').html(html); 

               updateElements();     
                }
            })
         })
        
                
    })(jQuery);
}


// =======
// bad section
//     (function($){
//         //wp-pagenavi add blue /green classes
//         $('.pagination-blue').each(function() {
//            $(this).find('.wp-pagenavi').addClass('pagenavi-blue');
//          });
//         $('.pagination-green').each(function() {
//            $(this).find('.wp-pagenavi').addClass('pagenavi-green');
//          });
//         var admin_ajax = home_url.url + "/wp-admin/admin-ajax.php";
//         //load news
//         $.post( admin_ajax, { action : 'ekFilterNews', query_vars: admin_ajax.query_vars }, function(html){
//         $('#ek-news-posts').html(html); 
//         }); 
//         //load events
//         $.post( admin_ajax, { action : 'ekFilterEvents', query_vars: admin_ajax.query_vars }, function(html){
//         $('#ek-events-posts').html(html);   
//         });
//         $(document).on('click','body', function(event){
//             $(".ek-suggest-dropdown").empty();
//             $('.search-ek').val('');
//         })
//         $(document).on('input','#search_ek', function(event){
//             event.preventDefault(); 
//             var search_value = $(this).val();
                    
//             $.ajax({
//                 url: admin_ajax,
//                 type: 'post',
//                 data: {
//                     action: 'ekSearchAllSite',
//                     searchQuery: search_value
//                 },
//                 dataType:'html',
//                 success: function(html) {
//                 $("#ek-search-results").html(html);
                    
//                 }
//             })
//          })
//         //get categories 
//         $(document).on('change', '#news-category-list, #events-category-list', function(){
//             window.category_id = $(this).val();                     
//         })
//         //get page number
//         function page_number(element){
//             element.find('page smaller').remove();
//             return parseInt( element.html() );
//         }
//         $(document).on('click','#filter-news-button, .pagination a', function(event){
//             event.preventDefault(); 
//             var page  = page_number($(this).clone());
            
//             if($('#events-category-list :selected').val() == ''){
//                 window.category_id = '';
//             }
            
//             var keyword = $('#news-keyword').val();
//             var date_from = $('#news-date-from').val();
//             var date_to = $('#news-date-to').val();
//             $.ajax({
//                 url: admin_ajax,
//                 type: 'post',
//                 data: {
//                     action: 'ekFilterNews',
//                     query_vars: admin_ajax.query_vars,
//                     keyword : keyword,
//                     date_from : date_from,
//                     date_to : date_to,
//                     category_id : window.category_id,
//                     page : page
//                 },
//                 dataType:'html',
//                 success: function(html) {
//                      // $('input').val('');
//                      // $('option').attr('selected', false);
                    
//                 $('#ek-news-posts').html(html);             
//                 }
//             })
//          })
// $(document).on('click','#filter-events-button, .pagination a', function(event){
//             event.preventDefault(); 
//             var page  = page_number($(this).clone());
            
//             if($('#events-category-list :selected').val() == ''){
//                 window.category_id = '';
//             }
            
//             var events_keyword = $('#events-keyword').val();
//             var events_date_from = $('#events-date-from').val();
//             var events_date_to = $('#events-date-to').val();
//             $.ajax({
//                 url: admin_ajax,
//                 type: 'post',
//                 data: {
//                     action: 'ekFilterEvents',
//                     query_vars: admin_ajax.query_vars,
//                     keyword : events_keyword,
//                     date_from : events_date_from,
//                     date_to : events_date_to,
//                     category_id : window.category_id,
//                     page : page
//                 },
//                 dataType:'html',
//                 success: function(html) {
//                      // $('input').val('');
//                      // $('option').attr('selected', false);
                    
//                 $('#ek-events-posts').html(html);           
//                 }
//             })
//   })
     


       
    
// }

