<!DOCTYPE html>
<html>
    <head>
        <title>Enterprise Kenya</title>
        <link rel="stylesheet" type="text/css" href="wp-content/themes/EnterpriseKenya/css/style.css">        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php wp_head(); ?>
    </head>
    <body>
        <nav class="main-nav">

            <a href="<?php echo get_home_url(); ?>">
                   <img src="<?= get_template_directory_uri(). '/img/enterprise-kenya.svg'; ?>" class="logo">    
            </a>
            <div id="nav-icon1">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <ul class="nav-menu">
                <li><a href="<?php echo get_permalink(get_page_by_title('news'));?>">news</a></li>
                <li><a href="<?php echo get_permalink(get_page_by_title('events'));?>">events</a></li>
                <li><a href="<?php echo get_permalink(get_page_by_title('faq'));?>">faq</a></li>
                <li><a href="<?php echo get_permalink(get_page_by_title('innovation'));?>">innovation</a></li>
                <li><a href="<?php echo get_permalink(get_page_by_title('contact'));?>">contact</a></li>
            </ul>            

            <div class="search-all-ek search--bar icon--new icon--arrow__right">
                <input type="search" id="search_ek" class="search-ek" placeholder="Search">                    
            </div>
            <div class="search-bar-autosuggest">
                 <datalist class="ek-suggest-dropdown" id="ek-search-results"></datalist> 
            </div>
        </nav>