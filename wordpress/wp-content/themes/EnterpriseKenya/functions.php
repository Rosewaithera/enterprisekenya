<?php

global $wpdb;  

include 'search.php';
include_once ('pages/news/news-filter.php');
include_once ('pages/events/events-filter.php');
include_once ('pages/innovation/innovation-filter.php');
include ('pages/news/news-overview.php');
include ('pages/events/events-overview.php');
include ('pages/innovation/innovation-overview.php');
include ('pages/innovation/apply-innovation.php');
include ('pages/contact/contact.php');
include ('pages/FAQ/faq.php');
include ('pages/FAQ/faq-filter.php');

require_once 'template/admin-manage-slider.php';
require_once 'template/slider-settings.php';
require_once 'template/admin-banner.php';
require_once 'template/ek-banner.php';
require_once 'template/admin-faq.php';

add_filter( 'wpcf7_validate_configuration', '__return_false' );


function enterprise_theme_support(){
  //Add featured image support in posts
  add_theme_support('post-thumbnails');
  add_image_size('small-thumbnail', 180, 120, true);
  add_image_size('banner-image', 920, 210, true);
  add_theme_support('post-formats', array('aside','gallery','link'));
}
add_action('after_setup_theme', 'enterprise_theme_support');

function excerpt($limit) {
    return wp_trim_words(get_the_excerpt(), $limit);
}

//get time difference for news
function time_ago( $type = 'post' ) {
    $d = 'comment' == $type ? 'get_comment_time' : 'get_post_time';
    return human_time_diff($d('U'), current_time('timestamp')) . " " . __('ago');

}

function enterprise_kenya_enqueue_scripts(){
    wp_enqueue_script('hamburger', get_template_directory_uri() . '/js/hamburger-menu.js', array(), '1.0.0', true);
    wp_enqueue_style('slider_style', get_template_directory_uri() . '/css/slider.min.css', array(), '1.0.0', 'all');
    wp_dequeue_style('contact-form-7');
    wp_enqueue_style('style_script', get_template_directory_uri() . '/css/style.min.css', array(), '1.0.0', 'all');
    wp_enqueue_script('count_down', get_template_directory_uri() . '/js/countDown.js', array(), '1.0.0', true);
    wp_enqueue_script('search_filter', get_template_directory_uri() . '/js/searchFilter.js', array('jquery'), '1.0.0', true);  
    wp_enqueue_script('faq_script', get_template_directory_uri() . '/js/faqAccordion.js', array('jquery'), '1.0.0', true); 
    wp_enqueue_script('search_filter', get_template_directory_uri() . '/js/searchFilter.js', array('jquery'), '1.0.0', true); 
    wp_enqueue_script('hamburger_script', get_template_directory_uri() . '/js/hamburger.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('slider_script', get_template_directory_uri() . '/js/slider.js', array(), '1.0.0', true); 
    wp_enqueue_script('datepicker', get_template_directory_uri() . '/js/datepicker.js', array('jquery'), '1.0.0', true);  

    
    $data = array('url' => __( get_home_url()));
    wp_register_script( 'search_filter', get_template_directory_uri() . '/js/searchFilter.js' );
    wp_localize_script( 'search_filter', 'home_url', $data );

    $toogle = get_page_by_title('toggle-slider', OBJECT, 'attachment');
    $toggle_data = array('toggle' =>  __( (string)$toogle->post_content));
    wp_register_script( 'slider_script', get_template_directory_uri() . '/js/searchAllContent.js' );
    wp_localize_script( 'slider_script', 'value', $toggle_data );//value.toggle

}

add_action( 'wp_enqueue_scripts', 'enterprise_kenya_enqueue_scripts' );

//query the homepage most recent posts
function query_EKposts($category_name, $sub_category_name, $posts_number){

  $category_id =  get_cat_ID($category_name);
  $sub_category_id =  get_cat_ID($sub_category_name);
  $query_posts = get_posts(array( 'post_type' => 'post', 'category' => $category_id, 'category__in' => $sub_category_id, 'posts_per_page' => $posts_number));

  return $query_posts;
}

function query_faq($post_type,$posts_number)
{
  $faq_post= get_posts(array( 'post_type' => 'faq', 'posts_per_page' => $posts_number));
  return $faq_post;
}

//Fetch the FEATURE_EVENT POST by category featured-event
function fetchFeatureEvent($categoryName){

    $category_id =  get_cat_ID($categoryName);
    $child_category = array( 'child_of' => $category_id);

    return $child_category;
}

 //stage ajax functions for search/filter functionality
add_action('wp_ajax_ekSearchAllSite', 'ekSearchAllSite');
add_action('wp_ajax_nopriv_ekSearchAllSite', 'ekSearchAllSite');
add_action('wp_ajax_ekFilterNews', 'ekFilterNews');
add_action('wp_ajax_nopriv_ekFilterNews', 'ekFilterNews');
add_action('wp_ajax_ekFilterEvents', 'ekFilterEvents');
add_action('wp_ajax_nopriv_ekFilterEvents', 'ekFilterEvents');
add_action('wp_ajax_ekFilterInnovation', 'ekFilterInnovation');
add_action('wp_ajax_nopriv_ekFilterInnovation', 'ekFilterInnovation');

add_action('wp_ajax_ekFilterFaq', 'ekFilterFaq');
add_action('wp_ajax_nopriv_ekFilterFaq', 'ekFilterFaq');

function enterprise_kenya_admin_enqueue_scripts() { 
   wp_enqueue_style( 'grid_css', get_template_directory_uri() . '/css/slider-admin-setting.min.css', array(), '1.0.0', 'all');
   wp_enqueue_script('toggle-slider', get_template_directory_uri() . '/js/toggleSlider.js', array(), '1.0.0', true); 
    
  }

add_action( 'admin_enqueue_scripts', 'enterprise_kenya_admin_enqueue_scripts');



add_action('wp_ajax_saveToggleValue', 'saveToggleValue');
add_action('wp_ajax_nopriv_saveToggleValue', 'saveToggleValue');
?>