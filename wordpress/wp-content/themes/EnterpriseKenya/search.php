<?php   
if (!function_exists('ekSearchAllSite')) {
	
	 function ekSearchAllSite(){

	   //get prerequisites here
	    global $wpdb;	   

	    $searchExpression = $_POST['searchQuery'];

	    $suggestions = '';
		
	    if(trim($searchExpression )){

		    $post_type = '%'. $wpdb->esc_like('p') . '%';
		    $post_trashed = '%'. $wpdb->esc_like('trashed') . '%';
		    $fanana = '%'. $wpdb->esc_like($searchExpression) . '%';  		    

		    $sql =  $wpdb->prepare( "SELECT * FROM $wpdb->posts WHERE ( post_type LIKE '%%%s%%' and post_name NOT LIKE '%%%s%%' and post_title LIKE '%%%s%%' or post_content LIKE '%%%s%%' )", $post_type, $post_trashed, $fanana, $fanana );
	 		
	 		$post_array = $wpdb->get_results($sql);	 		

	 		if(count($post_array) == 0 ){	 			
				$suggestions .= 		'<a href="'.get_home_url().'"><option>No results found...</option></a>';

	 		}else{ 	
	 			foreach ($post_array as $key => $row ) {

				    $data = array();
				    
				    if($key == 3 ) break; //3 suggestions

				    $get_post_title = get_the_title($row->ID);
				    $post_title = substr($get_post_title , 0, 32);
				    $permalink = get_permalink($row->ID);

				    $suggestions .= 		'<a href="'.$permalink.'"><option>'.$post_title.'...</option></a>';

			    }			    
		    }

	    }else{

			$suggestions .= 		'<a href="'.get_home_url().'"><option>No results found...</option></a>';	

	    }

    // HTML output
    header("Content-Type: application/html");    
    echo $suggestions;
    
    exit;

	}
}