<?php get_header(); ?>

    <div class="">
        <?php
            if(have_posts()):
            	while(have_posts()): the_post(); //for each post, do this
             		
        		    if(in_category( get_cat_ID('events'))){
        		    	include 'pages/events/events-single.php';
        		    }else if(in_category( get_cat_ID('news'))){
        		    	include 'pages/news/news-single.php';
        		    }else{       		  
             		 include 'pages/innovation/innovation-single.php'; 
                     }     
             	endwhile;   
            endif; 
        ?> 
    </div>  
<?php get_footer(); ?>