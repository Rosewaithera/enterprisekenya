<section class="slider-container" >
	<div class="slider-container">
		<div class="slider">			
			<?php 
				$loop = new WP_Query(array('post_type' => 'feature', 'posts_per_page' => -1, 'orderby'=> 'ASC')); 
			?>
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<div class="slide">
				    <div class="slider-img">
				        <?php the_post_thumbnail('full'); ?>
					</div>
					<div style="width:1350px; height:100px;  bottom:0; position:absolute;">
						<div class="slide-caption">
							<h2><?php the_title(); ?></h2>	
							<p><?php the_content();?></p>							
						</div>
						<div style="text-align:center; width: 900px; height: 30%;  margin-top: 60px; margin-left:240px;">
							<span class="dot" onclick="currentSlide(1)"></span>
							<span class="dot" onclick="currentSlide(2)"></span>
							<span class="dot" onclick="currentSlide(3)"></span>
					    </div>
					</div>					
				</div>

			<?php endwhile; ?>			
			<?php wp_reset_query(); ?>
		</div>		
	</div>
</section>