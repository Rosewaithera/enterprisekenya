<?php
	// register a custom post type called 'faq'
	add_action( 'init', 'ek_faq' );
	function ek_faq() {
	    $labels = array(
	        'name' => __( 'EK FAQS' ),
	        'singular_name' => __( 'faq' ),
	        'add_new' => __( 'New faq' ),
	        'add_new_item' => __( 'Add New faq' ),
	        'edit_item' => __( 'Edit faq' ),
	        'new_item' => __( 'New faq' ),
	        'view_item' => __( 'View faq' ),
	        'search_items' => __( 'Search faqs' ),
	        'not_found' =>  __( 'No faqs Found' ),
	        'not_found_in_trash' => __( 'No faqs found in Trash' ),
	    );
	    	$args = array(
		        'labels' => $labels,
		        'has_archive' => true,
		        'public' => true,
		        'hierarchical' => false,
			    'menu_position' => 100,
	        	'supports' => array(
	            'title',
	            'editor',s
	        ),	        
	    );
	    register_post_type( 'faq', $args );    
	}
?>