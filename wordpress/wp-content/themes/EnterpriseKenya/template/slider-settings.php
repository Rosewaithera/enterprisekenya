<?php
	add_action('init', 'create_feature');
	function create_feature() {
		$feature_args = array(
			'labels' => array(
				'name' => __( 'Add Sliders' ),
				'singular_name' => __( 'Add Slider' ),
				'add_new_item' => __('Manage Slider'),
				'add_new' => __( 'Add New Slider' ),
				'add_new_item' => __( 'Add New Slider' ),
				'edit_item' => __( 'Edit Slider' ),
				'new_item' => __( 'Add New Slider' ),
				'view_item' => __( 'View Slider' ),
				'search_items' => __( 'Search Slider' ),
				'not_found' => __( 'No Slider found' ),
				'not_found_in_trash' => __( 'No Slider found in trash' )
			),
			'public' => true,
			'show_in_menu' => 'manage-slider-settings',
			'show_ui' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'rewrite' => true,
			'menu_position' => 20,
			'supports' => array('title', 'editor', 'thumbnail')
		);
		register_post_type('feature',$feature_args);
	}
	add_filter("manage_feature_edit_columns", "feature_edit_columns");

	function feature_edit_columns($feature_columns){
		$feature_columns = array(
			"cb" => "<input type=\"checkbox\" />",
			"title" => "Title",
		);
		return $feature_columns;
	}

	function saveToggleValue(){
		$toggle_value = $_POST['toggle_value'];

		$data = array();

		if($toggle_value !== ''){
			$exist = get_page_by_title('toggle-slider', OBJECT, 'attachment');
			if(!$exist){

				$data['post_title'] = 'toggle-slider';
				$data['post_content'] = $toggle_value;
				$data['post_status'] = 'publish';
				$data['post_author'] = get_current_user_id();
				$data['post_type'] = 'attachment';

				wp_insert_post( $data );

			}else{
				$data['ID'] =  $exist->ID;
				$data['post_title'] = 'toggle-slider';
				$data['post_content'] = $toggle_value;
				$data['post_status'] = 'publish';
				$data['post_author'] = get_current_user_id();
				$data['post_type'] = 'attachment';

				wp_update_post( $data );
			}
		}
	}
?>