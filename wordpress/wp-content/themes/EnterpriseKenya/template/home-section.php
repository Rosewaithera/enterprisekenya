<section class="home-section">
    <div class="home-box home-box__text">
        <h1>Enterprise Kenya</h1>
        <p>Enterprise Kenya is a government organization that aims to nurture ICT innovations in the country. We work in partnership with the private sector to help ICT innovators start, grow, innovate and win export sales in global markets. It focuses on interventions at the various stages of innovation, from idea to growth to scaling stages. The entity has maximum impact in the innovation value chain. In this way, we support sustainable economic growth, regional development and secure employment.</p>
      
          <?php include __DIR__ . '/../news/news.php';          
          $news= get_page_by_title('news');
          ?>
        <a href="<?php echo get_permalink($news);?>"><button>ALL POSTS</button></a>                
    </div>
    <div  id ="clock-timer" class="home-box home-box__countdown">
        <ul class="home-box__countdown-blocks">
            <li class="home-box__countdown-block">
                <p class="days"></p>
                <span>days</span>
            </li>
            <li class="home-box__countdown-block">
                <p class="hours"></p>
                <span>hours</span>
            </li>
            <li class="home-box__countdown-block">
                <p class="minutes"></p>
                <span>minutes</span>
            </li>
            <li class="home-box__countdown-block">
                <p class="seconds"></p>
                <span>seconds</span>
            </li>
        </ul>
    </div>
</section>