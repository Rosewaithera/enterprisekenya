<?php
	function Ek_page_banner($page_id){
		$banner_args = array(
		 	 		'post_type' => 'banner',
				    'posts_per_page' => 10000,		   
				 );
		$banners = get_posts($banner_args);

		if($page_id === 'home'){
			$home_page = get_page_by_title($page_id);//get homepage id
			$page_id = $home_page->ID;
		}
		
		foreach ($banners as $banner) {
			$banner_page_id = get_post_meta($banner->ID, "banner_page", true);
				
			if($banner_page_id == $page_id){ 
				$banner_image = wp_get_attachment_image_src( get_post_thumbnail_id($banner->ID));	
				$banner_url = get_post_meta($banner->ID, "banner_url", true);

				?>
				<div class="container-img">
					<a target="_blank" href="<?= $banner_url ?>">
						<img class= "banner_image" src="<?= $banner_image[0] ?>" >				
				</div>
				<div class="quote-banner">
					<h2><?= $banner->post_title ?></h2>
					</a>
					<p>"<?= substr($banner->post_content, 0, 150).'...';?>"</p>
				</div>
				<div class="clearfix"></div>
				<?php
				break; //display 1 bannerthen break if > 1 banners
			}
		}
	}
?>