<?php
	// register a custom post type called 'banner'
	add_action( 'init', 'ek_banner' );
	function ek_banner() {
	    $labels = array(
	        'name' => __( 'EK Banners' ),
	        'singular_name' => __( 'banner' ),
	        'add_new' => __( 'New banner' ),
	        'add_new_item' => __( 'Add New banner' ),
	        'edit_item' => __( 'Edit banner' ),
	        'new_item' => __( 'New banner' ),
	        'view_item' => __( 'View banner' ),
	        'search_items' => __( 'Search banners' ),
	        'not_found' =>  __( 'No banners Found' ),
	        'not_found_in_trash' => __( 'No banners found in Trash' ),
	    );
	    	$args = array(
		        'labels' => $labels,
		        'has_archive' => true,
		        'public' => true,
		        'hierarchical' => false,
			'menu_position' => 100,
	        	'supports' => array(
	            'title',
	            'editor',
	            // 'excerpt',
	            // 'custom-fields',
	            'thumbnail',
	            //'page-attributes'
	        ),
	        //'taxonomies' => array( 'post_tag'),
	        'register_meta_box_cb' => 'add_banner_metaboxes'
	    );
	    register_post_type( 'banner', $args );    
	}	

	if( !function_exists('add_banner_metaboxes') ): 
	    function add_banner_metaboxes() {
	        add_meta_box('banner-select-page-section', __('Banner Settings', 'ek'), 'banner_page', 'banner', 'side', 'default');
	    }
	endif;

	if( !function_exists('banner_page') ): 
	    function banner_page($post) {
	        wp_nonce_field(plugin_basename(__FILE__), 'banners_noncename');
	        $mypost = $post->ID;
	        $originalpost = $post;
	        $page_list = '';
	        $selected_page = esc_html(get_post_meta($mypost, 'banner_page', true));
	        //$selected_page = '15';

	        $args = array(
	            'post_type' => 'page',
	            'post_status' => 'publish',
	            'posts_per_page' => -1
	        );

	        $page_selection = new WP_Query($args);

	        while($page_selection->have_posts()) {
	            $page_selection->the_post();
	            $the_id = get_the_ID();

	            $page_list .= '<option value="' . esc_attr($the_id) . '"';
	                if ($the_id == $selected_page) {
	                    $page_list .= ' selected';
	                }
	                $page_list .= '>' . get_the_title() . '</option>';
	        }

	        if(isset($_POST['banner_page'])) {
	            update_post_meta($post_id, 'banner_page', sanitize_text_field($_POST['banner_page']));
	        }

	        wp_reset_postdata();
	        $post = $originalpost;

	        print '
	            <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                <tr>
	                    <td width="50%" valign="top" align="left">
	                        <div class="adminField">
	                            <label for="banner_url">' . __('Banner URL', 'ek') . '</label><br />
	                            <input type="text" class="form-control" id="banner_url" name="banner_url" placeholder="' . __('Enter the banner URL', 'ek') . '" value="' . esc_attr(get_post_meta($post->ID, 'banner_url', true)) . '" />
	                        </div>
	                    </td>
	                </tr>
	                <tr>
	                    <td style="margin-top:15px;" width="50%" valign="top" align="left">
	                        <div class="form-control">
	                            <label for="banner_page">' . __('Assign a Page', 'ek') . '</label><br />
	                            <select id="banner_page" name="banner_page">
	                                <option value="">none</option>
	                                ' . $page_list . '
	                            </select>
	                        </div>
	                    </td>
	                </tr>	               
	            </table>';
	    }
	endif;

	if( !function_exists('ek_banner_meta_save') ): 
	    function ek_banner_meta_save($post_id) {
	        $is_autosave = wp_is_post_autosave($post_id);
	        $is_revision = wp_is_post_revision($post_id);
	        $is_valid_nonce = (isset($_POST['banners_noncename']) && wp_verify_nonce($_POST['banners_noncename'], basename(__FILE__))) ? 'true' : 'false';

	        if ($is_autosave || $is_revision || !$is_valid_nonce) {
	            return;
	        }
	        if(isset($_POST['banner_url'])) {
	            update_post_meta($post_id, 'banner_url', sanitize_text_field($_POST['banner_url']));
	        }
	        if(isset($_POST['banner_page'])) {
	            update_post_meta($post_id, 'banner_page', sanitize_text_field($_POST['banner_page']));
	        }
	        if(isset($_POST['banner_status'])) {
	            update_post_meta($post_id, 'banner_status', sanitize_text_field($_POST['banner_status']));
	        }

	        // now we can actually save the data
		$allowed = array( 
			'a' => array( // on allow a tags
				'href' => array() // and those anchors can only have href attribute
			)
		);
	    }
	endif;
	add_action('save_post', 'ek_banner_meta_save');
?>