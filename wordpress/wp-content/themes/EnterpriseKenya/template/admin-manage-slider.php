<?php

function slider_manage_page() {
    require_once 'slider-settings.php';
}

if( !function_exists('slider_menu') ):
    function slider_menu() {
        add_menu_page('EK settings', 'EK Sliders', 'manage_options','manage-slider-settings' , 'slider_manage_page');
        add_submenu_page('manage-slider-settings', 'Slider-Manage', 'Manage Slider', 'manage_options',  'manage-slider', 'ek_settings_page');
    }
endif;
add_action( 'admin_menu', 'slider_menu' );

if( !function_exists('ek_settings_init') ):
    function ek_settings_init() {
        wp_enqueue_style('font_awesome', get_template_directory_uri().'/css/font-awesome.css', false, '1.0', 'all');
        wp_enqueue_script('media-upload');
        wp_enqueue_script('settings', get_template_directory_uri().'/admin/js/admin.js', array('wp-color-picker'), '1.0', true);
        wp_localize_script('settings', 'settings_vars',
            array(
                'amenities_placeholder' => __('Add new', 'ek'),
                'admin_url' => get_admin_url(),
                'text' => __('Text', 'ek'),
                'numeric' => __('Numeric', 'ek'),
                'date' => __('Date', 'ek'),
                'no' => __('No', 'ek'),
                'yes' => __('Yes', 'ek'),
                'delete' => __('Delete', 'ek')
            )
        );
        register_setting( 'ek_general_settings', 'ek_general_settings' );
    }
endif;
add_action( 'admin_init', 'ek_settings_init' );

if( !function_exists('ek_admin_general_settings') ):
    function ek_admin_general_settings() {
        add_settings_section( 'ek_generalSettings_section', __( 'Home Page Slider Settings', 'ek' ), 'ek_general_settings_section_callback', 'ek_general_settings' );
        add_settings_field( 'ek_review_field', __( 'Enable EK Slider', 'ek' ), 'ek_review_field_render', 'ek_general_settings', 'ek_generalSettings_section' );
    }
endif;

if( !function_exists('ek_review_field_render') ):
    function ek_review_field_render() {
        $options = get_option( 'ek_general_settings' );
        ?>
        <input type="checkbox" name="ek_general_settings[slider_field]" <?php if(isset($options['slider_field'])) { checked( $options['slider_field'], 1 ); } ?> value="1">
        <span><strong>Check the box to enable the slider</strong></span>
        <?php
    }
endif;

if( !function_exists('ek_general_settings_section_callback') ):
    function ek_general_settings_section_callback() {
        echo '';
    }
endif;

if( !function_exists('ek_settings_page') ):
    function ek_settings_page() {
        $allowed_html = array();
        $active_tab = isset( $_GET[ 'tab' ] ) ? wp_kses( $_GET[ 'tab' ],$allowed_html ) : 'general_settings';
        $tab = 'ek_general_settings';

        switch ($active_tab) {
            case "general_settings":
                ek_admin_general_settings();
                $tab = 'ek_general_settings';
                break;
        }
        ?>
        <div class="ek-wrapper">            
            <div class="ek-content">
                <form action='options.php' method='post'>
                    <?php
                    wp_nonce_field( 'update-options' );
                    settings_fields( $tab );
                    do_settings_sections( $tab );
                    submit_button();
                    ?>
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
        <?php
    }
endif;
?>