<section class="single-page">
	<div class="container">
		<span class="green-title">
			<h3>events</h3>
		</span>	  			
		<img class="header-single-page" src="<?php the_post_thumbnail_url(); ?>">
		<div class="thumbnail-event-details">
			<span class="event-day"><?php the_time('j');?></span>
			<span class="event-month"><?php the_time('F');?></span>
		</div>
		<div class="single-page-box">
			<div class="flex-item left">
				<span class="green-title-short">
					<h2><?php the_title(); ?></h2>
				</span>
				<div class="meta-data-box">
					<p class="meta-data-date">posted : <?php the_time('j F Y');?></p>
					<br>
					<p class="meta-data-author">author : <?php the_author();?></p>
				</div>						
				<p class="font-size-single-page"><?php echo the_content(); ?></p>
				<!-- <div class="form-event-single-page"> -->
					<!-- <h3>Register for this event</h3> </div> -->
		 			<?php
	 						$register = get_page_by_title('contact-form-7-register');
	 						echo do_shortcode($register->post_content);
 						?>
 				
				<a href="<?php echo get_permalink(get_page_by_title('events'));?>">                   
                 <button class="green float-right event">All the events<i class="icn-btn arrow"></i></button>
                </a>
				
			</div>
			<div class="flex-item right">
				<?php  require_once __DIR__. '/../sidebar.php';?>
			</div>
		</div>
	</div>
</section>
