<?php
function enterprise_events_overview()
{
    $events_posts = query_EKposts('events', 'feature-event', 1);

  ?>

   <section>
        <div class="container">
            <span class="event-overview-title">
                <h3>featured event</h3>
            </span>   
        </div>
    </section>
    <?php
        foreach($events_posts as $post){
            $events_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID));
            $date_day = get_the_time('j', $post->ID);
            $date_month = get_the_time('F', $post->ID);
    ?>
    <div class="event-overview-intro">
        <div class="event-overview-box">        
            <div class="event-overview-leftbox">
            <img class= "news-image" src="<?= $events_image[0]; ?>" >
                <div class="date-month-container">
                    <span class="number"><?= $date_day ?></span>
                    <span class="month"><?= $date_month ?></span>
                </div>
            </div>
            <div class="event-overview-rightbox gradient-bg">
                <span class="event-box-maintitle"><h2><?=mb_strimwidth($post->post_title, 0, 25).'...'; ?></h2></span>
               <!--  <span class="event-box-subtitle"><h4>Subtitle of the event can be placed here</h4></span> -->
                <p><?= mb_strimwidth($post->post_content, 0, 150).'...'; ?></p>               
                <a href="<?= get_permalink($post->ID);?>">                   
                      <button class="green button-right-float">REGISTER FOR EVENT<i class="icn-btn arrow"></i></button>
                </a>
            </div>
            <div class="clearfix"></div>
        </div>    
    </div> 
    <?php } ?>   
    <section>
        <div class="container">
            <span class="green-title">
                <h3>UPCOMING EVENTS</h3>
            </span>   
            <div class="filter-section">
                <form>
                    <h3>Filter events by</h3>
                    <?php
                      $events_parent_category_id =  get_cat_ID('events');
                      $categories = get_categories(array('child_of' => $events_parent_category_id));
                      ?>                    
                        <input type="text" name="keywords" id='events-keyword' placeholder="Keywords...">
                        <div class="input-field-filter icon--new icon--arrow__right"> 
                    </div>
                    <div class="select-field icon--new icon--arrow__down">
                       <select class="" id="events-category-list">
                           <option value="" selected="selected">All categories</option>
                        <?php
                           foreach($categories as $child_category) { 
                              ?><option  value="<?= $child_category->cat_ID?>" ><?= $child_category->name ?></option>
                        <?php } ?>
                        </select>
                    </div>
                    <label class="real">            
                      <input type="text" class="datepicker" name="startdate" id="events-date-from" placeholder="Date from..">
                      <div class="select-calendar"> <span class ="icon--new icon--calendar"></span></div>
                    </label>

                    <label class="real">            
                      <input type="text" class="datepicker" name="enddate" id="events-date-to" placeholder="Date to..">
                      <div class="select-calender"> <span class ="icon--new icon--calendar"></span></div>
                    </label> 
                    <div class = "filter">
                        <button type="filter" class="red" id="filter-events-button">Filter Events<i class="icn-btn arrow"></i></button>
                    </div>
                </form>
            </div>
            <div class="" id="ek-events-posts"></div>
            <div class="pagination pagination-green">
                <?php
                $events_navi = query_EKposts('events', '', 10000);
                if(count($events_navi) > 10){
                    $events_query = new WP_Query(array('paged' => '','posts_per_page' => 10,));
                    wp_pagenavi( array('query' => $events_query)); 
                }
               ?>
            </div>   <!--  pagination using ajax          -->
          </div>
    </section>
     <section class="banner">    
      <?php 
      $events_page_id = get_the_ID();
      Ek_page_banner($events_page_id); 
      ?>
     </section>

<?php 
}
add_shortcode('enterprise-events-overview','enterprise_events_overview');// add this shortcode [enterprise-events-overview] in events page
?>