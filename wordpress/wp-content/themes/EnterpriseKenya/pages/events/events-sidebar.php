<section>	
	<div class ="the-sidebar">
		<span class="yellow-title">
			<h3>Share</h3>
		</span>
	  	<div class = "icon-container">
	  		<div class = "social-media-icon.yellow">
				<a href="#" class="icon--facebook social-media-icons yellow"></a>
		 		<a href="#" class="icon--twitter social-media-icons yellow"></a>
		 		<a href="#" class="icon--googleplus social-media-icons yellow"></a>
	 		</div>
	 	</div>
			<div class="highlight-content-sidebar">
			<div class="block latest-new">
				<span class="block-title">
					<h4>Latest news</h4>
				</span>				
				<div class="post-title-thumnail-sidebar">
					<h2 class="thumnail-title-sidebar"><a href="">Title of the post: lorem ipsum dolor sit amet.</a></h2>
				</div>
				<div class="post-title-thumnail-sidebar">
					<h2 class="thumnail-title-sidebar"><a href="">Title of the post: lorem ipsum dolor sit amet.</a></h2>
				</div>		
			</div>
		</div>
		<div class="block-title-upcoming-events">
			<span class="block-title-events">
				<h3>Next Upcoming events</h3>
			</span>
			<div class="highlight-content-sidebar">
				<div class="post-item-sidebar">
					<span class="thumbnail-sidebar">
						<div class="thumbnail-title-sidebar">
							<span>20</span>
							<span>July</span>
						</div>								
						<div class="event-content">
							<h2><a href="">New event</a></h2>
								<sub>Subtitle of the event</sub>
						</div>
					</span>
				</div>	
			</div>
		</div>		
	</div>		
</section>