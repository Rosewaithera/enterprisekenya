<?php 
//appears in every single page

	$news_posts = query_EKposts('news', '', 2);
	$events_posts = query_EKposts('events', '', 1);
	$innovation_posts = query_EKposts('innovation', '', 1);
?>
<section>
	<div class = "the-sidebar">
		<span class="yellow-title">
			<h3>Share</h3>
		</span>
	  	<div class = "icon-container">
	  		<div class = "social-media-icon.yellow">
				<a href="#" class="icon--facebook social-media-icons yellow"></a>
		 		<a href="#" class="icon--twitter social-media-icons yellow"></a>
		 		<a href="#" class="icon--googleplus social-media-icons yellow"></a>
	 		</div>
	 	</div>
			<div class="highlight-content-sidebar">
			<div class="block latest-new">
				<span class="block-title">
					<h4>Latest news</h4>
				</span>	
				<?php 
				foreach($news_posts as $post){
					$news_title = $post->post_title;
					$permalink = get_permalink($post->ID);
					$news_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID));
				?>
					<div class="post-title-thumnail-sidebar">
						<img class= "news-image" src="<?php echo $news_image[0]; ?>" >
						<h2 class="thumnail-title-sidebar"><a href="<?php echo $permalink; ?>"><?php echo substr($post->post_title , 0, 25).'...';?></a></h2>
					</div>

				<?php } ?>				
					
			</div>
		</div>
		
		<div class="block-title-upcoming-events">
			<span class="block-title-events">
				<h3>Next Upcoming events</h3>
			</span>
			<div class="highlight-content-sidebar">
				<!-- <div class="posts"> -->
				<?php
					foreach($events_posts as $post){
						$events_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID));
						$date_day = get_the_time('j', $post->ID);
						$date_month = get_the_time('F', $post->ID);
				?>
					<div class="post-item-sidebar">					
						<span class="thumbnail-sidebar">
							<img class= "news-image" src="<?php echo $events_image[0]; ?>" >
							<div class="thumbnail-title-sidebar">
								<span><?= $date_day; ?></span>
								<span><?= $date_month; ?></span>
							</div>								
							<div class="event-content">							
								<h2><a href="<?= get_permalink($post->ID);?>"><?= wp_trim_words($post->post_title  ,4); ?></a></h2>
								<sub><?= wp_trim_words($post->post_content  ,5); ?></sub>
							</div>
						</span>
					</div>
					<?php } ?>		
				<!-- </div> -->
			</div>
		</div>		
	</div>		
</section>
		

