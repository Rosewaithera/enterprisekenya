<?php
function enterprise_faq_page()
{
   $faq_posts = query_faq('faq', 10);

  ?>
  <section class="faq-section">
  	<span class="green-title">
      <h3>FREQUENTLY ASKED QUESTIONS</h3>
    </span>
		<div class = "filter-section">
      <form>
        <h3>Filter questions by</h3>
        <input type="text" name="keywords" id="faq-keyword" placeholder="Keywords...">
        <div class="input-field-filter icon--new icon--arrow__right"> </div>  
         <div class="select--field icon--new icon--arrow__down">
          <select class="">
            <option value="category1">Category...</option>
            <option value="category2">Category 2</option>
            <option value="category3">Category 3</option>
            <option value="category4">Category 4</option>
          </select>
        </div>     
        <button type="filter" id="filter-faq-button" class="red">FILTER QUESTIONS<i class="icn-btn arrow"></i></button>
      </form>
    </div>
        <div id="ek-faq-posts"></div>
    <div class="accordion">
      <?php
        $loop = new WP_Query(
          array(
            'post_type' => 'faq',
            'orderby'=>'date',
            'order'=>'ASC'
          )
        );
      ?>
      <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
        
      <div class="title-bars">
        <p class="bar-titles"><?php the_title(); ?></p>
        <div class="arrow-down icon--arrow__down"></div>
      </div>
        
      <div class="faq-content">
        <?php the_content();?>             
      </div>
        <?php endwhile; ?>
        <?php wp_reset_query(); ?>          
    </div>
  </section>
  <section class="banner">    
    <?php
    $faq_page_id = get_the_ID();
    Ek_page_banner($faq_page_id); 
    ?>
  </section>
  <?php 
}
add_shortcode('enterprise-faq-page','enterprise_faq_page');// add this shortcode [enterprise-faq-page] in FAQ page
?>