<?php

if (!function_exists('ekFilterFaq')) {
	/**
	 *
	 */
	function ekFilterFaq(){

	 		global $wpdb;

			$keyword = $_POST['keyword'];
			$paged = isset($_POST['page']) ? $_POST['page'] : 1 ;
			$faq_per_page = 10;


			$keyword = trim($keyword);
			$keyword_len = strlen($keyword);

			$valid_keyword = $keyword_len > 2;

			if($valid_keyword){
			 	 $filter_args = array(
			 			'post_type' => 'faq',
					    'paged' => $paged,
					    'posts_per_page' => $faq_per_page,
					    's' => $keyword,
					);

			}
			

			$filtered_faq = [];
			if($valid_keyword){
				$filtered_faq = get_posts($filter_args);
			}
			

			$html='';
			if(count($filtered_faq) == 0){


				$html.='<div class="post-item">';
				$html.=		'<div class="thumbnail">';
				$html.=		'</div>';
				$html.=		'<div class="content">';
				$html.=			'<span class="date"></span>';
				$html.=			'<span class="author text-right"></span>';
				$html.=			'<p>No results found...</p>';

				$html.=		'</div>';
				$html.='</div>';

			}else{


				foreach($filtered_faq as $faq) {

					$title = $faq->post_title;
				
			   		$content = $faq->post_content;

			   		$html .=' <div class="accordion ">';
					$html .=' <div class="title-bars ">';
        		    $html .=' <p class="bar-titles ">'. $title. '</p>';
                    $html .=' <div class="arrow-down icon--arrow__down"></div>';
                    $html .=' </div>';
                    $html .=' <div class="faq-content ">';
                    $html .=''.$content.'';          
                    $html .='   </div>';
                    $html .='   </div>';



				}
			}

		header("Content-Type: application/html");
	     echo $html;


	    exit;
	}
}
?>