<?php
/**
 * @package WordPress
 * @subpackage EK
 */
$ek_general_settings = get_option('ek_general_settings');
$slider = isset($ek_general_settings['slider_field']) ? $ek_general_settings['slider_field'] : '';
$news_posts = query_EKposts('news', '', 2);
$events_posts = query_EKposts('events', '', 2);
?>
<main>
    <?php if($slider != '') { ?>
        <section class="slider">
            <div class="ek-slider">
                <?php
                $loop = new WP_Query(
                    array(
                        'post_type' => 'feature',
                        'posts_per_page' => -1,
                        'orderby'=> 'ASC'
                    )
                );
                $number_of_images = count($loop->posts);
                ?>
                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                    <div class="slide">
                        <div class="container-img">
                            <img class= "slider_image" src="<?php echo the_post_thumbnail_url('full'); ?>" >
                        </div>
                        <div class="container--img__width">
                            <div class="quote-slider">
                                <h2><?php the_title(); ?></h2>
                                <p>"<?php echo substr(get_the_content(), 0, 150).'...';?>"</p>
                            </div>
                            <div class="slider-controllers" >
                                <?php for($i = 1; $i<= $number_of_images; $i++){ ?>
                                    <span class="dot" onclick="currentSlide(<?php echo $i;?>)"></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php wp_reset_query(); ?>
            </div>
        </section><!-- Ends Section Slider -->
    <?php } ?>
    <section class="highlight-content">
     <div class="container">
            <div class="block latest-news">
                <span class="block-title">
                    <h3>Latest news</h3>
                </span>           
                <div class="posts">
                    <?php
                      foreach($news_posts as $post){
                        // $news_title = $post->post_title;
                        $newsPermalink = get_permalink($post->ID);
                        $news_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID));
                        $author_name = get_the_author_meta('display_name', $post->post_author);  
                        $date = get_the_time('j F Y', $post->ID);
                    ?>
                    <div class="post-item">
                        <div class="thumbnail">
                          <img class= "news-image" src="<?php echo $news_image[0]; ?>" >
                            <h2 class="thumbnail-title"><a href="<?php echo  $newsPermalink;?>"><?php echo substr($post->post_title , 0, 25).'...';?></a></h2>
                        </div>
                    </div>
                 <?php } ?>                    
                </div> <!-- Ends Posts -->              
                 <a href="<?php echo get_permalink(get_page_by_title('news'));?>">                   
                   <button class="home-btn blue">ALL THE POSTS<i class="icn-btn arrow"></i></button>
                 </a>
            </div> <!-- Ends Block Latest News-->
        
          <div class="block upcoming-events">

           <span class="block-title">
                        <h3>LATEST EVENTS</h3>
                    </span>

                    <div class="posts">
                    <?php
                    foreach($events_posts as $post){
                        $eventPermalink = get_permalink($post->ID);
                        $events_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID));
                        $author_name = get_the_author_meta('display_name', $post->post_author);  
                        $date = get_the_time('j F Y', $post->ID);
                    ?>
                   
                        <div class="post-item">
                            <div class="thumbnail">
                              <img class= "news-image" src="<?php echo $events_image[0]; ?>" >
                                <h2 class="thumbnail-title"><a href="<?php echo  $eventPermalink;?>"><?php echo substr($post->post_title , 0, 25).'...';?></a></h2>
                            </div>                                    
                        </div>
                    </div>
                    <?php } ?>    
                     <a href="<?php echo get_permalink(get_page_by_title('events'));?>">                   
                         <button class="home-btn green home-btn-events">ALL THE POSTS<i class="icn-btn arrow"></i></button>
                    </a>
            </div> <!-- Ends Block Upcoming Events -->
                 
     </div> <!-- Ends Container-->
    </section> <!-- Ends Section Higlight Content -->
   
    <section class="banner">
      <?php $home_page_id = "home";//add a home  page identifier
        Ek_page_banner($home_page_id); 
      ?>
    </section>
    <section class="companies">
        <div class="">
            <a href="https://www.microsoft.com" class="link" target="_blank"><img src="<?= get_template_directory_uri(). '/img/microsoft.png'; ?>" class="com-logo microsoft"></a>
        </div>
        <div class="">
            <a href="https://www.google.com" class="link" target="_blank"><img src="<?= get_template_directory_uri(). '/img/google.jpg'; ?>" class="com-logo google"></a>
        </div>
        <div class="">
            <a href="https://www.oracle.com" class="link" target="_blank"><img src="<?= get_template_directory_uri(). '/img/oracle_logo.jpg'; ?>" class="com-logo oracle"></a>
        </div>
        <div class="">
            <a href="http://www.competa.com" class="link" target="_blank"><img src="<?= get_template_directory_uri(). '/img/competa.png'; ?>" class="com-logo competa"></a>
        </div>
        <div class="clearfix"></div>
    </section>
</main>