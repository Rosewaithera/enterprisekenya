<?php
	if (!function_exists('ekFilterInnovation')) {
		function ekFilterInnovation(){

	 		global $wpdb;

	 		$query_vars = $_POST['query_vars'];
			$keyword = $_POST['keyword'];
			$date_from = $_POST['date_from'];
			$date_to = $_POST['date_to'];
			$child_category_id = $_POST['category_id'];
			$paged = isset($_POST['page']) ? $_POST['page'] : 1 ;
			$events_per_page = 10;

			$parent_category_id =  get_cat_ID('innovation');

			if(!$keyword &&  !$date_from &&  !$date_to&& !$child_category_id){
				$filter_args = array(
			 	 		'post_type' => 'post',
					    'category' => $parent_category_id,
					    'paged' => $paged,
					    'posts_per_page' => $innovation_per_page,
					    'query_vars' =>  $query_vars,
					 );
			}

			if(trim($keyword)){
			 	 $filter_args = array(
			 			'post_type' => 'post',
					    'category' => $parent_category_id,
					    'paged' => $paged,
					    'posts_per_page' => $innovation_per_page,
					    's' => $keyword,
					);

			}
			if($child_category_id !== ""){
			 	 $filter_args = array(
			 			'post_type' => 'post',
					    'category' => $parent_category_id,
					    'paged' => $paged,
					    'posts_per_page' => $innovation_per_page,
					    'category__in' => $child_category_id,
					);
			}
			if(!empty($date_from) && !empty($date_to)){
			 	 $filter_args = array(
			 			'post_type' => 'post',
			 			'category' => $parent_category_id,
			 			'paged' => $paged,
			 			'posts_per_page' => $innovation_per_page,
					    'date_query' => array(
						   array(
						            'after'     => $date_from,
						            'before'    => $date_to,
						            'inclusive' => true,
						        ),
						),
					);
			 }


			$filtered_innovation= get_posts($filter_args);

			$html='';
			if(count($filtered_innovation) == 0){


				$html.='<div class="post-item">';
				$html.=		'<div class="thumbnail">';
				//$html.=			'<h2 class="thumbnail-title"></h2>';
				$html.=		'</div>';
				$html.=		'<div class="content">';
				$html.=			'<span class="date"></span>';
				$html.=			'<span class="author text-right"></span>';
				$html.=			'<p>No results found...</p>';

				$html.=		'</div>';
				$html.='</div>';

			}else{

				foreach($filtered_innovation as $innovation) {

					$title = $innovation->post_title;
					$title = substr($title, 0, 25).'...';
			   		$content = $innovation->post_content;
			   		$post_image = wp_get_attachment_image_src( get_post_thumbnail_id($innovation->ID));
			   		$permalink = get_permalink($innovation->ID);
			   		$author_name = get_the_author_meta('display_name', $innovation->post_author);
			   		$date = get_the_time('j F Y', $innovation->ID);

					$html.='<div class="post-item">';
					$html.=		'<div class="post-title-thumbnail">';
					$html.=			'<img class= "news-image" src="'.$post_image[0].'" >';
					$html.=			'<div class="title-container green">';
					$html.=			'<h2 class="post-thumbnail-title"><a href="'.$permalink.'"><br>'.$title.'</a></h2>';
					$html.=			'</div>';
					$html.=		'</div>';
					$html.=		'<div class="content gradient-bg">';
					$html.=		'<div class="post-author-container">';
					$html.=			'<span class="post-date">'.$date.'</span>';
					$html.=			'<span class="author-text">by '.$author_name.'</span>';
					$html.=		'</div>';
					$html.=			'<p>'.substr($content , 0, 190).'...</p>';
					$html.=			'<a href="'.$permalink.'"><button class="green button-right-float">Read more<i class="icn-btn arrow"></i></button></a>';
					$html.=		'</div>';
					$html.=		'<div class="clearfix"></div>';
					$html.='</div>';
				}
			}
		     header("Content-Type: application/html");
		     echo $html;
				// header("Content-Type: application/json");
				// echo json_encode($data);
		    exit;
		}
	}
?>