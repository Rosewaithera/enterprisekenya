<?php

	$events_posts = query_EKposts('events', '', 1);
	$innovation_posts = query_EKposts('innovation', '', 2);
	?>
<section>	
	<div class = "the-sidebar">
		<span class="yellow-title">
			<h3>Share</h3>
		</span>
	  	<div class = "icon-container">
	  		<div class = "social-media-icon.yellow">
				<a href="#" class="icon--facebook social-media-icons yellow"></a>
		 		<a href="#" class="icon--twitter social-media-icons yellow"></a>
		 		<a href="#" class="icon--googleplus social-media-icons yellow"></a>
	 		</div>
	 	</div>
			<div class="highlight-content-sidebar">
			<div class="block latest-new innovation">
				<span class="block-title innovate">
					<h4>Innovations</h4>
				</span>
				<?php 
				foreach($innovation_posts as $post){
					$innovation_title = $post->post_title;
					$permalink = get_permalink($post->ID);
					$innovation_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID));
				?>				
				<div class="post-title-thumnail-sidebar">
					<img class= "news-image" src="<?php echo $innovation_image[0]; ?>" >
					<h2 class="thumnail-title0-sidebar"><a href="<?php echo $permalink; ?>"><?php echo substr($post->post_title , 0, 25).'...';?></a></h2>
				</div>

				<?php } ?>		
			</div>
		</div>
		<div class="block-title-upcoming-events">
			<span class="block-title-events">
				<h3>Next Upcoming events</h3>
			</span>
			<div class="highlight-content-sidebar">
				<!-- <div class="posts"> -->
				<?php
					foreach($events_posts as $post){
						$events_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID));
						$date_day = get_the_time('j', $post->ID);
						$date_month = get_the_time('F', $post->ID);
				?>
					<div class="post-item-sidebar">
						<span class="thumbnail-sidebar">
							<div class="thumbnail-title0-sidebar">
								<span><?= $date_day; ?></span>
								<span><?= $date_month; ?></span>
															
								<div class="event-content">							
								<h2><a href="<?= get_permalink($post->ID);?>"><?= wp_trim_words($post->post_title  ,4); ?></a></h2>
								<sub><?= wp_trim_words($post->post_content  ,5); ?></sub>
								</div>
							</div>
						</span>
					</div>
					<?php } ?>		
				<!-- </div> -->
			</div>
		</div>		
	</div>		
</section>