<?php
function enterprise_apply_innovation(){
    
  $innovation =  get_page_by_title('innovation');
?>
<section class="innovation-single">		
	<div class="container">
        <span class="block-title">
            <h3>
        <?php       
      $key = 'innovation-title';
      echo get_post_meta($innovation->ID, $key, true);
      ?></h3>
        </span>			
		<p>
      <?php 
      $key = 'innovation-content';
      echo get_post_meta($innovation->ID, $key, true);
      ?>.</p>
        <span class="block-title">
            <h3>apply your innovation</h3>
        </span>		 
 		<?php 
	 		$apply_innovation =  get_page_by_title('contact-form-7-innovation-form'); 
	 		echo do_shortcode( $apply_innovation->post_content ); 
 		?>                   	  		
	</div>
</section>
<?php
}
add_shortcode('enterprise-apply-innovation','enterprise_apply_innovation');
// add this shortcode [enterprise-apply-innovation] in apply-innovation
?>