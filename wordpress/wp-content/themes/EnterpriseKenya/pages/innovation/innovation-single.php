<section class="single-page">
	<div class="container">
		<span class="green-title">
			<h3>innovation</h3>
		</span>		
		<img class="header-single-page" src="<?php the_post_thumbnail_url(); ?>">	
		<div class="single-page-box">
			<div class="flex-item left">
				<span class="green-title-short">
					<h2><?php the_title(); ?>.</h2>
				</span>
				<div class="meta-data-box">
					<p class="meta-data-date">posted : <?php the_time('j F Y');?></p>	
					<p class="meta-data-author">author : <?php the_author();?></p>
				</div>	

				<p class="font-size-single-page"><?php echo the_content(); ?></p>

				<div class="innovation-box">
					<div class="box-left">
						<span>Company:</span>
						<span>Website:</span>
						<span>Company adres:</span>
						<span>Place of operation:</span>
						<span>Email adress:</span>
						<span>Level of Innovation or ICT startup:</span>
					</div>
					<div class="box-right">
						<span>Competa</span>
						<span><a class="green-link">www.competa.com</a></span>
						<span>Streetname 325</span>
						<span>Haarlem</span>
						<span>email@competa.com</span>
						<span>9000</span>
					</div>
				</div>
				<a href="<?php echo get_permalink(get_page_by_title('innovation'));?>">    
				<button class="green float-right innovation">
					All the Innovations<i class="icn-btn arrow"></i>
				</button></a>
			</div>
			
			<div class="flex-item right">
				 <?php  require_once __DIR__. '../innovation-sidebar.php';?>
			</div>
		</div>
	</div>
</section>