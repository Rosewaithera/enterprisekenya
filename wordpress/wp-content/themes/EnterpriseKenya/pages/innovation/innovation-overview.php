<?php
function enterprise_innovation_overview()
{


  $innovation =  get_page_by_title('innovation');
?>
  <section class="banner">
      <?php include_once __DIR__. '/../../template/ek-banner.php'; ?>
  </section>

<section>
	<div class="container">
		<span class="green-title">
				<h3>
        <?php       
      $key = 'innovation-title';
      echo get_post_meta($innovation->ID, $key, true);
      ?></h3>
		</span>
		<div class="intro-box">
					
			<p>
      <?php 
      $key = 'innovation-content';
      echo get_post_meta($innovation->ID, $key, true);
      ?>
      </p>
      <a href="<?php echo get_permalink(get_page_by_title('apply-innovation'));?>">                   
       	<button class="green innovation-btn">Apply your innovation<i class="icn-btn arrow"></i></button>
      </a>
		</div>
		<span class="green-title">
			<h3>recently joined innovations</h3>
		</span>
        <div class="posts" id="ek-innovation-posts"></div><!-- all posts come here using ajax -->

        <div class="pagination pagination-green">
          <?php
            $innovation_navi = query_EKposts('innovation', '', 10000);
            if(count($innovation_navi) > 10){
              $innovation_query = new WP_Query(array('paged' => '','posts_per_page' => 10,));
              wp_pagenavi(array('query' => $innovation_query)); 
            }
          ?>
        </div> 										

	</div>
</section>

<section class="banner">    
  <?php 
  $innovation_page_id = get_the_ID();
  Ek_page_banner($innovation_page_id); 
  ?>
</section>

<?php 
}
add_shortcode('enterprise-innovation-overview','enterprise_innovation_overview');
// add this shortcode [enterprise-apply-innovation] in innovation page
?>