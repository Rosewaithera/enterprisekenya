<?php

  function enterprise_news_overview()


  {
      $news_posts = query_EKposts('news', 'feature-news', 1);
    ?>
    
    <section>
      <div class="container">
        <span class="blue-title">
          <h3>news</h3>
        </span> 
        <div class = "filter-section">
          <form>
            <h3>Filter news by</h3>
            <?php
            $parent_category_id =  get_cat_ID('news');
            $categories = get_categories(array('child_of' => $news_parent_category_id));
            ?>
              <input type="text" name="keywords" id="news-keyword" placeholder="Keywords...">
              <button class="input-field-filter icon--new icon--arrow__right"> 
            </button>
            <div class="select-field icon--new icon--arrow__down">
              <select class="" id="news-category-list">
                 <option value="" selected="selected">All categories</option>

                <?php
                 foreach($categories as $child_category) { 

                    ?><option  value="<?= $child_category->cat_ID?>" ><?= $child_category->name ?></option><?php            
                 }


               ?>
            </select>
          </div>
          <label class="real">            
            <input type="text" class="datepicker" name="startdate" id="news-date-from" placeholder="Date from..">
            <div class="select-calendar"> <span class ="icon--new icon--calendar"></span></div>
          </label>

          <label class="real">            
            <input type="text" class="datepicker" name="enddate" id="news-date-to" placeholder="Date to..">
            <div class="select-calender"> <span class ="icon--new icon--calendar"></span></div>
          </label> 
          
          
          <div class = "filter">
            <button type="filter" class="red" id="filter-news-button" >Filter News<i class="icn-btn arrow"></i></button>
          </div>
        </form>
      </div>

        <div class="posts" id="ek-news-posts"></div><!-- all posts come here using ajax -->

        <div class="pagination pagination-blue">
          <?php
            $news_navi = query_EKposts('news', '', 10000);
            if(count($news_navi) > 10){
              $news_query = new WP_Query(array('paged' => '','posts_per_page' => 10,));
              wp_pagenavi(array('query' => $news_query)); 
            }
          ?>
        </div> 
      </div>
    </section>

    <section class="banner">    
      <?php
        $news_page_id =  get_the_ID();
        Ek_page_banner($news_page_id); 
      ?>
    </section>
    <?php
  }
  add_shortcode('enterprise-news-overview','enterprise_news_overview');// add this shortcode [enterprise-news-overview] in news page
?>