<section class="single-page">
    <div class="container">
        <span class="blue-title">
            <h3>News</h3>
        </span>             
        <img class="header-single-page" src="<?php the_post_thumbnail_url(); ?>"> 
        <div class="single-page-box">
            <div class="flex-item left">
                <span class="blue-title-short">
                    <h2><?php the_title(); ?></h2>
                </span>
                <div class="meta-data-box">
                    <p class="meta-data-date">posted : <?php the_time('j F Y');?></p> 
                    <br>
                    <p class="meta-data-author">author : <?php the_author();?></p>
                </div>      
                <p class="font-size-single-page"><?php echo the_content(); ?></p> 
                <?php  $news= get_page_by_title('news');?>
                <a href="<?php echo get_permalink($news);?>">                   
                 <button class="blue float-right">All the news<i class="icn-btn arrow"></i></button>
                </a>
            </div>
            <div class="flex-item right">
                <?php  require_once __DIR__. '/../sidebar.php';?>
            </div>
        </div>
    </div>
</section> 