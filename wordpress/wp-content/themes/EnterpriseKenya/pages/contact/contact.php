<?php
function enterprise_contact_page()
{
  ?>
  <section class="banner">
      <?php include_once __DIR__. '/../../template/ek-banner.php'; ?>
  </section>
  <section class="contact-section">
	<div class="contact-left">
	<span class="contact-title blue-title"><h3>contact</h3></span>		
		<div class="google-maps"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.813258878317!2d36.81719181416919!3d-1.2860755990619757!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f10d0cdc6fef3%3A0xf4b17fc532cb6b56!2sICT+Authority!5e0!3m2!1sen!2ske!4v1485503355885" width="100%" height="338" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</div>
	<div class="contact-right">	
	<span class="contact-title blue-title"><h3>ask a question</h3></span>	
        <!-- <form action = "" method = "post" class="innovation-form"> -->
 		
 			<?php
	 			$contact = get_page_by_title('contact-form-7-contact-form');
	 			echo do_shortcode($contact->post_content);
 			?>
 			<!-- <button class = "red"> Send message<i class ="icn-btn arrow"></i></button> -->

 			<!-- </form> -->
	</div>
	<div class="clearfix"></div>
</section>
<section class="banner">    
  <?php 
  $contact_page_id = get_the_ID();
  Ek_page_banner($contact_page_id); 
  ?>
</section>
<?php 
}
add_shortcode('enterprise-contact-page','enterprise_contact_page');// add this shortcode [enterprise-contact-page] in contact page
?>