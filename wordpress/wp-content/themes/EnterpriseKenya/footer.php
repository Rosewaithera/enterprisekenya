		<footer class="footer">		
			<?php  wp_footer();?> 
			<div class="footer-copyright">
				<span class="copyright-content">Copyright Enterprise Kenya</span>
			</div>
				<div class ="footer-logo">
					<a href="/"><img class="big" src="<?php bloginfo('stylesheet_directory'); ?>/img/ftsf-logo.png"></a>
				</div>
			<div class="icons-container">
				<a href="#" class="icon--facebook social-media-icons grey"></a>
				<a href="#" class="icon--twitter social-media-icons grey"></a>
				<a href="#" class="icon--googleplus social-media-icons grey"></a>
			</div>
		</footer>
	</body>
	<script src="//localhost:35729/livereload.js"></script>
</html>