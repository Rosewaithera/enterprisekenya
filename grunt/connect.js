module.exports = {
    server: {
        options: {
            base: './',
            livereload: true,
            open: {
                target: 'http://localhost:8080/enterprise-kenya/wordpress/wp-content/themes/EnterpriseKenya/pages/index.php'
            }
        }
    }
}