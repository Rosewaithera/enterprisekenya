module.exports = {
    options: {
        livereload: true
    },
    html: {
        files: ['wordpress/wp-content/themes/EnterpriseKenya/*.php'],
        tasks: ['htmlhint']
    },
    js: {
        files: ['js/*.js'],
        tasks: ['uglify', 'jshint']
    },
    css: {
        files: ['scss/**'],
        tasks: ['sass', 'cssmin']
    }
}