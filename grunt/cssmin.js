module.exports = {
   target: {
        files: [{
            expand: true,
            cwd: 'wordpress/wp-content/themes/EnterpriseKenya/css/',
            src: ['*.css', '!**.min.css'],
            dest: 'wordpress/wp-content/themes/EnterpriseKenya/css/',
            ext: '.min.css'
        }]
    }
}
