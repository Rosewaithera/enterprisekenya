CREATE THE FOLLOWING FIRST

#Index/Landing Page

This Page has a slider at the top and a banner at the bottom, the add pictures for the slider, go th admin dashboard, e.g. ```enterprise-kenya/wordpress/wp-admin/``` 
and navigate on the left hand side to a menu called ``EK Sliders``, click add new slider and add an image of your choosing, preferably of big dimensions
```200X200``` pixels, the reason for this is if a smaller image is uploaded, it will stretch and look horrendous, so bigger images a preferable to maintain their quality, add relevant text in the 
text area and set featured image on the right hand side.

###NOTE:

The other pictures appearing for the LATEST NEWS and NEXT UPCOMING EVENTS appear based on what has been uploaded in their respective pages, which
will be explained further on this document.

#NEWS page

wp-admin, on the left hand side, click posts `>>` Add New `>>` Name the post `>>` assign the post a *category* of ```news``` for it to appear
 in the news page, (if you have not created a category called news, make sure this category is created). >> set featured image , click *publish* .
 This post will appear on the News page as the latest news.
 
#Events page
wp-admin, on the left hand side, click posts `>>` Add New `>>` Name the post `>>` assign the post a *category* of ```events``` for it to appear 
in the events page, >> set featured image , click *publish* . This post will appear on the events page as an event.

For the events page, you can set an event as a normal event or ```featured event``` , to set the featured event, during publishing the post,
set the category of the post to events then sub-category to ```feature-event```. 

 #FAQ page
 Has a banner only at the bottom, assigned in EK Banners.
 
 #INNOVATION page
 
 Not implemented at the moment.
