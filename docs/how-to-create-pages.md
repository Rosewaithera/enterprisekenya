#SHORT CODE ---- PAGE NAME TO CREATE

[enterprise-news-overview]--          news

[enterprise-apply-innovation]--       apply-innovation

[enterprise-faq-page]--               faq

[enterprise-events-overview]--        events

[enterprise-contact-page]--           contact

[enterprise-innovation-overview]--    innovation

[//contact form 7 shortcode//]--      contact-form-7-innovation-form

[//contact form 7 shortcode//]--      contact-form-7-contact-form

[//contact form 7 shortcode//]--      contact-form-7-register

##### 1. apply-innovation
create contact form in `contact form 7 plugin` called apply-innovation and add the code below....
```
[text* full-name class:form-full-width class:name-icon-orange placeholder"Name"]

[text* innovation class:form-full-width placeholder"Name of Innovation"]

[textarea* service_product class:icon--new class:innovation-textarea class:form-full-width  maxlength:400 placeholder"Describe your product/service"]
<div class="select--field icon--new icon--arrow__down form-full-width align-items">
[select* owner_type  class:form-full-width  placeholder"Founder / Owner" "Founder/Owner" "Co-founder" ]
</div>
[text* company_name class:form-full-width placeholder"Name of Company"]
[text* employee_number class:form-full-width placeholder"Size/ number of employees"]
[text* established_year class:form-full-width placeholder"When it was established"]
<div class="select--field icon--new icon--arrow__down form-full-width align-items">
[select* category class:form-full-width  placeholder"Category / Sector" "Finance & Financial Services" "Education & Training" "Agriculture" "Social Equity" "Water & Sanitation" "Environment" "Health Care Delivery" "Housing & Urbanization" "Gender" "Youth & Vulnerable Groups" "Wholesale & Retail Trade" "Manufacturing" "Tourism" "BPO" "Others"]
[textarea* requirements class:innovation-textarea class:form-full-width placeholder"What do you require? : (state problem and your requirement)"]
</div>
[text* website class:form-full-width placeholder"Website"]
[text* email class:form-full-width placeholder"E-mail address"]
[text* mobile class:form-full-width placeholder"Contact number"]
[acceptance accept-this class:innovation-checkbox]<span class="checkbox-text">I accept the terms and conditions*</span>
<div class="survey-navigation">
[submit class:green class:float-right "Submit"]
</div>
```

##### 1. contact form in contact page
create contact form in `contact form 7 plugin` called contact-form-contact and add the code below....
paste the shortcode generated in contact-form-7-contact-form page. 
Add the html class as: html_class = "innovation-form".
```
[text* Name class:form-full-width placeholder"Name"]
[text* Company class:form-full-width placeholder"Company"]
[text* Email class:form-full-width placeholder"Email Address"]
[text* Phone number class:form-full-width placeholder"Phone number"]
[textarea* Message class:innovation-textarea form-full-width placeholder"Message"]
<button class = "red" type = "submit"> Send message<i class ="icn-btn arrow"></i></button>
```

##### 1. register form in events single page
create contact form in `contact form 7 plugin` called contact-form-7-register and add the code below....
Add the shortcode generated in contact-form-7-register
Add the html class as: html_class = "form-event-single-page".
```
<h3>Register for this event</h3>
[text* Name class:form-full-width placeholder"Name"]
[text* Email class:form-full-width placeholder"Email"]
[text* Phone Number class:form-full-width placeholder"Phone number"]
<button type="submit" class="red">register<i class="icn-btn arrow"></i></button>
```
